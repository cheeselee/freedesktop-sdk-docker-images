#define _GNU_SOURCE
#include <dlfcn.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <errno.h>

extern int
connect(int sockfd, const struct sockaddr *addr,
        socklen_t addrlen)
{
  if (addr->sa_family == AF_INET) {
    const struct sockaddr_in *inet_addr = (const struct sockaddr_in *)addr;
    if ((ntohl(inet_addr->sin_addr.s_addr) >> (8*3)) != 127) {
      errno = ECONNREFUSED;
      return -1;
    }
  } else if (addr->sa_family == AF_INET6) {
     const struct sockaddr_in6 *inet6_addr = (const struct sockaddr_in6 *)addr;
    if (inet6_addr->sin6_addr.s6_addr[15] != 1) {
      errno = ECONNREFUSED;
      return -1;
    }
    for (unsigned i = 0; i < 15; ++i) {
      if (inet6_addr->sin6_addr.s6_addr[i] != 0) {
        errno = ECONNREFUSED;
        return -1;
      }
    }
  }

  int (*original)(int, const struct sockaddr *, socklen_t);
  original = dlsym(RTLD_NEXT, "connect");
  return original(sockfd, addr, addrlen);
}

